jQuery.fn.exists = function() {
	return this.length > 0;
};
// to align the elements height
// container .box
// elements .box-img and .box-content
// call $(element).box();
// call $(element).box('refresh');
+function ($) {
	'use strict';
	
	var self;
	
	var Box = function(element, options) {
		self = this;
		self.options = '';
		self.$element = null;
		self.isShown = false;
		self.init('box', element, options);
		self.windowWidth = $(window).width();
		self.windowHeight = $(window).height();
	};
	
	Box.prototype.init = function(type, element, options) {
		self.$element = $(element);
		var $elementChild = self.$element.find('.box');
		
		this.addArray($elementChild, self.$element);
	};
	
	Box.prototype.addArray = function(element, parent) {
		var elementArrayTmp = [],
				elementArray = [];
		var $items = element,
				$itemsImg = $items.children('.box-img'),
				$itemsContent = $items.children('.box-content');
		
		self.isShown = true;
		self.destroy($items);

		var i = 0,
				j = 0,
				len = $items.length;
		for (i; i < len; i++) {
			if($itemsImg.length !== 0){
				if($itemsImg[i].offsetHeight !== 0)
					elementArrayTmp.push($itemsImg[i].offsetHeight);
			}
			elementArray.push($itemsContent[i].offsetHeight);
		}
		
		$items.each(function(key, element) {
			if($(window).width() <= 760) return false;
			$(this).css('height', elementArray.max() + elementArrayTmp.min() + 'px');
			$itemsImg.css({
				'height': elementArrayTmp.max() + 'px',
				'overflow': 'hidden'
			});
			$itemsContent.css({
				'height': (elementArray.max()) + 'px',
				'transition': 'all 0.3s'
			});
		});
		
		self.resize();
	};
	
	Box.prototype.destroy = function(element) {
		var element = (element) ? element : self.$element.find('.box');
		var $items = element,
				$itemsImg = $items.children('.box-img'),
				$itemsContent = $items.children('.box-content');
		$items.removeAttr('style');
		$itemsImg.removeAttr('style');
		$itemsContent.removeAttr('style');
	};
	
	Box.prototype.refresh = function() {
		var triggers = self.$element.attr('class').split(' ');
		var parent = $('.' + triggers[triggers.length - 1]);
		$.each(parent, function(){
			if($(this).is(':visible')){
				var $elementChild = $(this).find('.box');
				self.addArray($elementChild, this);
			}
		});
	};
	
	Box.prototype.resize = function () {
		if (self.isShown) {
			$(window).on('resize.bs.box', $.proxy(self.handleUpdate, this));
		} else {
			$(window).off('resize.bs.box');
		}
	};
	
	Box.prototype.handleUpdate = function(element) {
		var windowWidthNew = $(window).width();
		var windowHeightNew = $(window).height();
		if(self.windowWidth != windowWidthNew || self.windowHeight != windowHeightNew){
			self.windowWidth = windowWidthNew;
			self.windowHeight = windowHeightNew;
			self.refresh.call();
		}
	};
	
	Array.prototype.max = function() {
		return Math.max.apply(null, this);
	};

	Array.prototype.min = function() {
		return Math.min.apply(null, this);
	};

	function Plugin(option) {
		return this.each(function() {
			var $this = $(this);
			var data = $this.data('bs.box');
			var options = typeof option == 'object' && option;
			
			if($(this).is(':visible')){
				if (!data) $this.data('bs.box', (data = new Box(this, options)));
				if (typeof option == 'string') data[option]();
			}
		});
	}

	$.fn.box = Plugin;
	$.fn.box.Constructor = Box;

}(jQuery);

var slider = false;

Utilities = {
	mobile: function() {
		var check = false;
		if (/Android|webOS|iPhone|iPod|iPad|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			check = true;
		} else {
			check = false;
		}
		return check;
	}
};

touchMenu = {
	shown: function(){
		$('body').addClass('modal-open');
		$('#navbar').collapse('show');
		$('.app').append('<div class="modal-backdrop fade" href="#"></div>').add($('.modal-backdrop').addClass('in'));
		//phoneSliderMob.slider.slick("getSlick").refresh();
	},
	hidden: function(){
		$('body').removeClass('modal-open');
		$('#navbar').collapse('hide');
		$('.modal-backdrop').css('display', 'none').remove();
	},
	init: function(){
		$(document).on('click.bs.collapse', '.navbar-top-toggle', touchMenu.shown);
		$(document).on('click.bs.collapse', '.app-header-nav .navbar-toggle', touchMenu.hidden);
		$(document).on('click', '.modal-backdrop', function(){
			if($('.collapse.in').is(':visible')){
				$('.app-header-nav .navbar-toggle').trigger('click');
			}
		});
	}
};

catalogMenu = {
	mouseoverchage: 0,
	leftAnimate: ["72", "292", "502", "660"],
	mobileSubmenuToggle: function(e){
		var $this = $(e.currentTarget);
		var parentEl = $this.parents('.catalog');
		var parentNav = $this.parents('.catalog-nav');
		var index = $this.parent().index();
		var self = $this.parent().children('ul');
		
		parentNav.find('.catalog-nav__item.active').removeClass('active');
		$('.catalog-subnavbar').not(self).slideUp();
		if(self.is(':visible')){
			self.slideUp();
		} else {
			self.slideDown();
			$this.parent().addClass('active');
		}
		
		parentEl.find('.catalog-pane__item.in').removeClass('active in');
		parentEl.find('.catalog-pane__item').eq(index).addClass('active in');
		//$(".catalog-nav__arrow").css({"left": catalogMenu.leftAnimate[index]+"px"});
	},
	pcSubmenuToggle: function(e){
		var $this = $(e.currentTarget);
		var parentEl = $this.parents('.catalog');
		var parentNav = $this.parents('.catalog-nav');
		var index = $this.parent().index();
		parentNav.find('.catalog-nav__item.active').removeClass('active');
		$this.parent().addClass('active');
		parentEl.find('.catalog-pane__item.in').removeClass('active in');
		parentEl.find('.catalog-pane__item').eq(index).addClass('active in');
		//$(".catalog-nav__arrow").stop(true, false).animate({"left": catalogMenu.leftAnimate[index] +"px" } , 500);
	},
	init: function(){
		$(document).on('click', '.catalog-nav-target', function(e){
			if((Utilities.mobile() && $(window).width() <= 768) || $(window).width() <= 768){
				catalogMenu.mobileSubmenuToggle(e);
			} else {
				catalogMenu.pcSubmenuToggle(e);
			}
		});
		if($(window).width() > 768){
			$('.catalog-nav__item:eq(0) .catalog-nav-target').trigger('click');
		}
	}
};

productsSlider = {
	slider: $('.products-top-list'),
	init: function(){
		if($('.products-top-list').exists()){
			productsSlider.slider.slick({
				slidesToScroll: 1,
				slidesToShow: 4,
				dots: false,
				arrows: false,
				infinite: false,
				speed: 400,
				responsive: [
					{
						breakpoint: 1280,
						settings: {
							slidesToScroll: 3,
							slidesToShow: 3,
							dots: true
						}
					},
					{
						breakpoint: 1024,
						settings: {
							slidesToScroll: 2,
							slidesToShow: 2,
							dots: true
						}
					},
					{
						breakpoint: 560,
						settings: {
							slidesToScroll: 1,
							slidesToShow: 1,
							arrows: false,
							dots: true
						}
					}
				]
			});
		}
	}
};

Popup = {
	hideModal: function(){
		if($('.collapse.in').is(':visible')){
			$('.modal-backdrop').fadeOut(function() {
				this.remove();
			});
			$('#popup-feedback').collapse('hide');
			$('body').removeClass('modal-open');
		}
	},
	
	shownModal: function(){
		$('body').addClass('modal-open');
		$('#popup-feedback').collapse('show').before('<div class="modal-backdrop fade"></div>').add($('.modal-backdrop').css('background-color', '#ffffff').addClass('in'));

		$(document).on('click.bs.collapse', '.modal .close', function() {
			Popup.hideModal();
		});
	}
};

scrollToId = {
	toId: function(id){
		$('html, body').animate({ scrollTop: parseInt($(id).offset().top) }, 1000);
	},
	
	init: function(){
		$('.js-scrollTo').on('click', function(e){
			var $this = $(e.currentTarget);
			var dataProd = $this.data('prod');
			if(dataProd !== ""){
				$("#prod_summary").val(dataProd).trigger("change");
			} else {
				$("#prod_summary").val("").trigger("change");
			}
			scrollToId.toId($this.attr('href'));
		});
	}
};

catalogSort = {
	toggle: function(e){
		var $this = $(e.currentTarget);
		var parentEl = $this.parents('.sort');
		if(parentEl.find('.sort__item').size() > 1) {
			if(!$this.hasClass('active')){
				parentEl.find('.active').removeClass('active');
				$this.addClass('active');
			} else {
				return false;
			}
		} else {
			$this.toggleClass('active');
		}
		
	},
	init: function(){
		$(document).on('click', '.sort-target', catalogSort.toggle);
	}
};

contentMenu = {
	hidden: function(){
		if($('.wrap-content__right.collapse').hasClass('in')){
			$('.modal-backdrop').fadeOut(function() {
				this.remove();
			});
			$('.wrap-content__right').collapse('hide');
			$('body').removeClass('modal-open');
		}
	},
	shown: function(e){
		var $this = $(e.currentTarget);
		if($this.attr("href")) e.preventDefault();
		$('body').addClass('modal-open');
		$('.section__content').after('<div class="modal-backdrop fade"></div>').add($('.modal-backdrop').css('background-color', '#000').addClass('in'));
		$('.wrap-content__right').collapse('show').addClass('in');
		$(document).on('click.bs.collapse', '.section .modal-backdrop', function() {
			contentMenu.hidden();
		});
	},

	init: function(){
		$(document).on('click', '.js-menu-collapse', contentMenu.shown);
		$(document).on('click', '.js-menu-hidden', contentMenu.hidden);
	}
}

formEdit = {
	edit: function(e){
		var $this = $(e.currentTarget);
		var parentElem = $this.parents('.form-fieldset');
		var showElem = parentElem.find('.js-form-enable');
		var lockElem = parentElem.find('.js-form-disable');
		if($this.attr("href")) e.preventDefault();
		$this.hide().add(lockElem.hide()).add(showElem.show());
	},

	lock: function(e){
		var $this = $(e.currentTarget);
		if($this.attr("href")) e.preventDefault();
		var parentElem = $this.parents('.form-fieldset');
		var showElem = parentElem.find('.js-form-enable');
		var lockElem = parentElem.find('.js-form-disable');
		parentElem.find('.js-form-edit').show().add(lockElem.show()).add(showElem.hide());
	},

	init: function(){
		$(document).on('click', '.js-form-edit', formEdit.edit);
		$(document).on('click', '.js-form-lock', formEdit.lock);
	}
};

$(function() {
	touchMenu.init();
	catalogMenu.init();
	productsSlider.init();
	scrollToId.init();
	catalogSort.init();
	contentMenu.init();
	formEdit.init();
	
	$('.js-company-add-trigger').on('click', function(){
		$('.js-company-add').hide().add($('.js-company-fields').show());
	});

	if ($('.tab').exists()) {
		$('.tab-nav').each(function(){
			var handler = $(this).children(".tab-nav__item").eq(0);
			if(handler != 0){
				handler.children('.tab-nav-handler').tab('show');
			}
		});
		
		$('.tab-nav__item .tab-nav-handler').on('click', function(e) {
			var $this = $(e.currentTarget);
			if($this.attr("href")) e.preventDefault();
			$this.tab('show');
			if($this.parents('.tab-cover.dropdown').length){
				var $parents = $this.parents('.tab-cover.dropdown');
				var dataText = $this.data('text');
				$parents.find('.filter-option').empty().text(dataText);
			}
		});
		
		var masActiv = {'#tab-1':"false"};
		$('.tab-nav__item .tab-nav-handler').on('shown.bs.tab', function (e) {
			var index = $(e.target).attr('href');
			if(masActiv[index] != 'false'){
				if($('.rate-slider').exists()){
					$('.rate-slider:visible').each(function(){
						$(this).slick("getSlick").refresh();
					});
				}
				$('.js-fixed-height').box('refresh');
				masActiv[[index]] = 'false';
			}
		});
		
		$('.js-tab-open').on('click', function(e){
			var $this = $(e.currentTarget);
			if($this.attr("href")) e.preventDefault();
			$('.tab-nav-handler[href="' + $this.attr("href") + '"]').trigger('click');
		});
	}
	
	if($('.animate-images').exists()){
		var posEl = $('.animate-images').offset().top;
		$(window).scroll(function(){
			var scrollTop = $(window).scrollTop();
			if(scrollTop >= posEl){
				$('.animate-images .conclusion__media').addClass('in');
			}
		});
	}
	
	$(document).on('click', '.js-print', function(e){
		window.print();
	});
	
	// if($('.agree .scrollbar').exists()){
	// 	$(".agree .scrollbar").scroller({
	// 		handleSize: 30
	// 	});
	// }
	
	if($('.js-fixed-height').exists() && $('.js-fixed-height').is(':visible')){
		$('.js-fixed-height').box();
	}
	
});
