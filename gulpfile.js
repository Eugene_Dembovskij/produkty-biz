// requires

var gulp = require('gulp'),
    zip = require('gulp-zip'),
    del = require('del'),
    sass = require('gulp-sass'),
    util = require('gulp-util'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    coffee = require('gulp-coffee'),
    autoprefixer = require('gulp-autoprefixer'),
    combineMq = require('gulp-combine-mq'),
    browserSync = require('browser-sync'),
    imagemin = require('gulp-imagemin'),
    cssbeautify = require('gulp-cssbeautify'),
    csscomb = require('gulp-csscomb'),
    jade = require('gulp-jade'),
    runSequence = require('run-sequence'),
    spritesmith = require('gulp.spritesmith'),
    svgSprite = require('gulp-svg-sprite'),
    svg2png = require('gulp-svg2png'),
    size = require('gulp-size'),
    reload = browserSync.reload;

// configuration

var basePaths = {
	src: 'source/',
	dest: 'html/f/',
};

var paths = {
  source: 'source',
  css: 'source/f/css',
  scss: 'source/f/scss',
  js: 'source/f/js',
  img: 'source/f/i',
  sprites: 'source/f/sprites',
  svg: 'source/f/svg',
  fonts: 'source/f/fonts/**/*',
  content: 'source/content',
  releases: 'releases',
  tpl: 'source/tpl',
  images: {
		src: basePaths.src + 'f/i/',
		dest: basePaths.dest + 'i/'
	},
	sprite: {
		src: basePaths.src + 'f/sprite/*',
		svg: 'i/sprite.svg',
		css: '../../' + basePaths.src + 'scss/_sprite.scss'
	}
};

watch = {
  jade: paths.source + '/**/*.jade',
  css: paths.css + '/**/*.css',
  scss: paths.scss + '/**/*.scss',
  js: paths.js + '/**/*.js',
  coffee: paths.js + '/**/*.coffee',
  html: paths.source + '/**/*.html',
  img: paths.img + '/**/*',
  content: paths.content + '/**/*',
};

dest = {
  source: 'html',
  css: 'html/f/css',
  scss: 'html/f/css',
  js: 'html/f/js',
  img: 'html/f/i',
  fonts: 'html/f/fonts',
  content: 'html/content',
  all: 'html/**/*'
};

options = {
  autoprefixer: [
    'last 2 version',
    'safari 5',
    'opera 12.1',
    'ios 6',
    'android 4'
  ],
  imageopt: {
    progressive: true,
    svgoPlugins: [{removeViewBox: false}]
  }
};

isProduction = false;

var changeEvent = function(evt) {
  util.log('File', util.colors.cyan(evt.path.replace(new RegExp('/.*(?=/' + paths.source + ')/'), '')), 'was', util.colors.magenta(evt.type));
};
// browser sync

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: dest.source
    },
    online: false,
    open: false
  });
});

// reloading  -->  .pipe(reload({stream: true}));

// sprites

gulp.task('sprite', function generateSpritesheets () {
  var spriteData = gulp.src(paths.sprites+'/*.png')
    .pipe(spritesmith({
      retinaSrcFilter: paths.sprites+'/*@2x.png',
      imgName: 'sprites.png',
      retinaImgName: 'sprites@2x.png',
      cssName: '_sprites.scss',
      cssFormat: 'scss',
      padding: 10,
      imgPath: '../i/sprites.png',
      retinaimgPath: '../i/sprites@2x.png',
      algorithm: 'binary-tree',
      cssVarMap: function (sprite) {
        sprite.name = 'sprite-' + sprite.name;
      }
    }));
  spriteData.img.pipe(gulp.dest(dest.img));
  spriteData.css.pipe(gulp.dest(paths.scss));
});

gulp.task('svgSprite', function () {
  return gulp.src(paths.sprite.src)
    .pipe(svgSprite({
      shape: {
        spacing: {
          padding: 5
        }
      },
      mode: {
        css: {
          dest: "./",
          layout: "diagonal",
          sprite: paths.sprite.svg,
          bust: false,
          render: {
            scss: {
              dest: '../../'+paths.scss+"/_svg_sprite.scss",
              template: paths.tpl+'/sprite-template.scss'
            }
          }
        }
      },
      variables: {
        mapname: "icons"
      }
    }))
    .pipe(gulp.dest(basePaths.dest));
});

gulp.task('pngSprite', ['svgSprite'], function() {
  return gulp.src('html/f/i/sprite.svg')
    .pipe(svg2png())
    .pipe(size({
      showFiles: true
    }))
    .pipe(gulp.dest(paths.images.dest));
});

gulp.task('svg', ['pngSprite']);

// styles

gulp.task('scss', function() {
  return gulp.src(paths.scss+'/*.*')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(autoprefixer(options.autoprefixer))
    .pipe(cssbeautify({
      indent: '	',
      autosemicolon: true
    }))
    .pipe(csscomb())
    .pipe(isProduction ? combineMq() : util.noop())
    .pipe(gulp.dest(dest.scss))
    .pipe(reload({stream: true}));
});

gulp.task('css', function() {
  return gulp.src(paths.css+'/**/*.*')
  .pipe(gulp.dest(dest.css))
  .pipe(reload({stream: true}));
});

// scripts

gulp.task('modernizr', function() {
  return gulp.src(paths.js+'/head/modernizr.min.js')
  .pipe(gulp.dest(dest.js));
});

gulp.task('jquery', function() {
  return gulp.src(paths.js+'/head/jquery.min.js')
  .pipe(gulp.dest(dest.js));
});

gulp.task('fancybox', function() {
  return gulp.src(paths.js+'/fancybox/**/*.js')
  .pipe(gulp.dest(dest.js));
});

gulp.task('vendorjs', function() {
  return gulp.src(paths.js+"/vendor/*.js")
    .pipe(uglify())
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest(dest.js))
    .pipe(reload({stream: true}));
});

gulp.task('userjs', function() {
  return gulp.src(paths.js+"/*.js")
    .pipe(gulp.dest(dest.js))
    .pipe(reload({stream: true}));
});

gulp.task('browserify', function () {
  return browserify(paths.js+'/app.js').bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(dest.js));
});

// images

gulp.task('img', function() {
  return gulp.src(paths.img+'/**/*.*')
  .pipe(imagemin(options.imageopt))
  .pipe(gulp.dest(dest.img))
  .pipe(reload({stream: true}));
});

// copying statics

gulp.task('fonts', function() {
  return gulp.src(paths.fonts)
  .pipe(gulp.dest(dest.fonts));
});

gulp.task('contents', function() {
  return gulp.src(paths.content+'/**/*.*')
  .pipe(gulp.dest(dest.content))
  .pipe(reload({stream: true}));
});

// html

gulp.task('html', function() {
  return gulp.src(paths.source+'/*.html')
    .pipe(gulp.dest(dest.source))
    .pipe();
});

// jade

gulp.task('jade', function() {
  return gulp.src(paths.source+'/*.jade')
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest(dest.source))
    .pipe(reload({stream: true}));
});

// clean

gulp.task('clean', function() {
  return del([
    dest.source,
    paths.releases
  ]);
});

// default

gulp.task('default', ['build']);

// build

gulp.task('build', ['clean'], function() {
  runSequence(
    'sprite',
    ['css', 'scss', 'vendorjs', 'userjs', 'jquery', 'modernizr', 'fancybox', 'img', 'svg', 'fonts', /*'html',*/ 'jade', 'contents'],
    'watch', function() {}
  );
});

// watch

gulp.task('watch', ['browser-sync'], function() {
  gulp.watch(watch.css, ['css']);
  gulp.watch(watch.scss, ['scss']);
  gulp.watch(watch.js, ['vendorjs', 'userjs']);
  gulp.watch(watch.img, ['img']);
  gulp.watch(watch.fonts, ['fonts']);
  gulp.watch(watch.jade, ['jade']);
  //gulp.watch(watch.html, ['html']);
  gulp.watch(watch.content, ['contents']);
  /*gulp.watch(paths.img+'/sprite.svg', ['svg']).on('change', function(evt) {
    changeEvent(evt);
  });*/
});

// zip

gulp.task('zip', /*['build'], */function () {
  return gulp.src(dest.all)
    .pipe(zip('build.zip'))
    .pipe(gulp.dest(paths.releases));
});
